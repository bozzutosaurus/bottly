import discord
import typing

from bottly.discord_bot_plugin import DiscordBotMessagePlugin
from bottly.discord_plugin_actions import DiscordPluginAction


class DiscordPluginManager:

    def __init__(self):
        self.plugins: typing.List[DiscordBotMessagePlugin] = []

    def add_plugin(self, plugin: DiscordBotMessagePlugin):
        self.plugins.append(plugin)

