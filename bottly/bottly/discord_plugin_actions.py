import abc
import discord
import pathlib

from discord.ext.commands import Context


class DiscordPluginAction(abc.ABC):

    @abc.abstractmethod
    def perform(self, context: Context):
        pass


class PostMessage(DiscordPluginAction):
    def __init__(self, message: str, format_as_codeblock: bool = True):
        self.message = message
        if format_as_codeblock:
            self.message = f'```{self.message}```'

    async def perform(self, context: Context):
        await context.channel.send(self.message)


class UploadFile(DiscordPluginAction):
    def __init__(self, file_path: pathlib.Path, attachment_name: str = None):
        self.file_path = file_path
        self.attachment_name = attachment_name

    async def perform(self, context: Context):
        with open(self.file_path, 'rb') as file_to_upload:
            await context.channel.send(file=discord.File(file_to_upload, filename=self.attachment_name))


class DownloadFile(DiscordPluginAction):
    def __init__(self, attachment: discord.Attachment, file_path: pathlib.Path):
        self.attachment = attachment
        self.file_path = file_path

    async def perform(self, context: Context):
        with open(self.file_path, 'wb') as download_file:
            await self.attachment.save(download_file)
