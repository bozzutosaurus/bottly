import typing

from discord.ext import commands
from bottly.discord_plugin_actions import DiscordPluginAction


class DiscordBotMessagePlugin(commands.Cog):

    @staticmethod
    async def perform_actions(actions: typing.List[DiscordPluginAction], context: commands.Context):
        for action in actions:
            await action.perform(context)
