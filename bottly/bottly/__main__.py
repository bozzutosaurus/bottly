import os
import pkg_resources

from discord.ext import commands

bot = commands.Bot('/')


@bot.event
async def on_ready():
    print(f'We have logged in as {bot}')


if __name__ == '__main__':
    modules = {entrypoint.name: entrypoint.load() for entrypoint in pkg_resources.iter_entry_points('bottly.plugins')}
    plugins = []
    for plugin_name, plugin_module in modules.items():
        print(f'Loading {plugin_name}...')
        if not hasattr(plugin_module, 'get_plugins'):
            print(f'{plugin_name} has no get_plugins method')
            continue
        new_plugins = plugin_module.get_plugins(bot)
        print(f'Loaded {len(new_plugins)} from {plugin_name}.')
        plugins += new_plugins

    for plugin in plugins:
        bot.add_cog(plugin)

    bot.run(os.environ['DISCORD_CLIENT_TOKEN'])
