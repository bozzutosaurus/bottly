from setuptools import setup

setup(
    name='bottly',
    version='1.0',
    description='A framework to easily add functionality to a discord bot',
    author='Dom Bozzuto',
    author_email='domeniclbozzuto@gmail.com',
    packages=['bottly'],
    requires=['discord']
)