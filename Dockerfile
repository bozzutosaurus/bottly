FROM python:latest

WORKDIR /app

COPY . /app/discord-bot-framework

RUN pip install --upgrade pip setuptools
RUN pip install -r discord-bot-framework/bottly/requirements.txt
RUN pip install -e discord-bot-framework/bottly
RUN pip install -r discord-bot-framework/bottly-dnd-plugin/requirements.txt
RUN pip install -e discord-bot-framework/bottly-dnd-plugin

CMD ["python", "-m", "bottly"]