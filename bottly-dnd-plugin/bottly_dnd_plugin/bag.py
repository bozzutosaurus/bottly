from discord.ext import commands
from bottly.discord_bot_plugin import DiscordBotMessagePlugin
from bottly.discord_plugin_actions import PostMessage
from bottly_dnd_plugin.model.bag.bag import Bag
from bottly_dnd_plugin.model.bag.command_runner import BagCommandRunner
from bottly_dnd_plugin.model.bag.interpreter.lexer import BagCommandLexer
from bottly_dnd_plugin.model.bag.interpreter.parser import BagCommandParser


class BagMessagePlugin(DiscordBotMessagePlugin):

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.bag = Bag()
        self.bag_command_lexer = BagCommandLexer()
        self.bag_command_parser = BagCommandParser()
        self.bag_command_runner = BagCommandRunner(self.bag)

    @commands.command(name='bag')
    async def bag(self, context: commands.Context):
        command = self.bag_command_parser.parse(self.bag_command_lexer.tokenize(context.message.content))
        command_result = self.bag_command_runner.run(command)
        await self.perform_actions([PostMessage(command_result.output)], context)
