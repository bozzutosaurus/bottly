import abc
import dataclasses
import enum
import math
import random
import typing


class RollArithmetic(enum.Enum):
    PLUS = enum.auto()
    MINUS = enum.auto()

    def symbol(self):
        if RollArithmetic(self.value) == RollArithmetic.PLUS:
            return '+'
        else:  # self.value == RollArithmetic.MINUS
            return '-'


class RollModifier(enum.Enum):
    NONE = enum.auto()
    ADVANTAGE = enum.auto()
    DISADVANTAGE = enum.auto()


class RollNumericComponent(abc.ABC):

    def __init__(self, raw_value: str):
        self.raw_value = raw_value
        self.resolved_value: int = 0

    def roll(self) -> int:
        """
        Resolves the numeric component to an integer value
        """


class DiceComponent(RollNumericComponent):

    def __init__(self, raw_value: str):
        super().__init__(raw_value)
        count, sides = self.raw_value.split('d')
        self.count = int(count)
        self.sides = int(sides)

    def roll(self) -> int:
        self.resolved_value = sum([math.ceil(random.random() * self.sides) for _ in range(self.count)])
        return self.resolved_value


class ConstantComponent(RollNumericComponent):

    def __init__(self, raw_value: str):
        super().__init__(raw_value)
        self.value = int(self.raw_value)

    def roll(self) -> int:
        self.resolved_value = self.value
        return self.resolved_value


@dataclasses.dataclass
class RollResult:

    result: int
    unresolved_expression: str = None
    resolved_expression: str = None


class Roll:

    def __init__(self, components: typing.List[typing.Tuple[RollArithmetic, RollNumericComponent]]):
        self.components = components
        self.unresolved_expression = ' '.join([f'{arithmetic.symbol()} {numeric.raw_value}'
                                               for (arithmetic, numeric) in self.components])[2:]

    def perform(self) -> RollResult:
        result = 0
        resolved_expression = ''

        for arithmetic, numeric in self.components:
            evaluated_roll = numeric.roll()
            resolved_expression += f'{arithmetic.symbol()} {evaluated_roll} '
            if arithmetic == RollArithmetic.PLUS:
                result += evaluated_roll
            else:  # arithmetic == RollArithmetic.MINUS
                result -= evaluated_roll
        return RollResult(result, resolved_expression=resolved_expression[2:],
                          unresolved_expression=self.unresolved_expression)

    def perform_n_times(self, n: int) -> typing.List[RollResult]:
        return [self.perform() for _ in range(n)]
