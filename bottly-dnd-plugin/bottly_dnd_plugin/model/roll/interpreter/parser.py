import typing

from bottly_dnd_plugin.model.roll.command_runner import RollCommand
from bottly_dnd_plugin.model.roll.interpreter.lexer import RollCommandLexer
from bottly_dnd_plugin.model.roll.roll import ConstantComponent, DiceComponent, RollArithmetic, \
    RollModifier, RollNumericComponent

from sly import Parser

'''
roll : COMMAND_KEYWORD expression
     | COMMAND_KEYWORD expression WITH_KEYWORD modifier

expression : numeric
           | expression arithmetic numeric

numeric : NUMBER
        | DICE

arithmetic : PLUS
          | MINUS
        
modifier : ADVANTAGE_KEYWORD
         | DISADVANTAGE_KEYWORD
'''


# noinspection PyUnresolvedReferences,PyMethodMayBeStatic
class RollCommandParser(Parser):
    tokens = RollCommandLexer.tokens

    @_('roll : COMMAND_KEYWORD expression')
    def roll_expression(self, p) -> RollCommand:
        return RollCommand(p.expression)

    @_('roll : COMMAND_KEYWORD expression WITH_KEYWORD modifier')
    def roll_expression_with_modifier(self, p) -> RollCommand:
        return RollCommand(p.expression, modifier=p.modifier)

    @_('expression : numeric')
    def expression_numeric(self, p) -> typing.List[typing.Tuple[RollArithmetic, RollNumericComponent]]:
        return [(RollArithmetic.PLUS, p.numeric)]

    @_('expression : expression arithmetic numeric')
    def expression_expression_arithmetic_numeric(self, p) -> typing.List[typing.Tuple[RollArithmetic,
                                                                                      RollNumericComponent]]:
        return p.expression + [(p.arithmetic, p.numeric)]

    @_('numeric : NUMBER')
    def numeric_number(self, p) -> RollNumericComponent:
        return ConstantComponent(p.NUMBER)

    @_('numeric : DICE')
    def numeric_dice(self, p) -> RollNumericComponent:
        return DiceComponent(p.DICE)

    @_('arithmetic : PLUS')
    def artimetic_plus(self, p) -> RollArithmetic:
        return RollArithmetic.PLUS

    @_('arithmetic : MINUS')
    def artimetic_plus(self, p) -> RollArithmetic:
        return RollArithmetic.MINUS

    @_('modifier : ADVANTAGE_KEYWORD')
    def modifier_advantage(self, p) -> RollModifier:
        return RollModifier.ADVANTAGE

    @_('modifier : DISADVANTAGE_KEYWORD')
    def modifier_disadvantage(self, p) -> RollModifier:
        return RollModifier.DISADVANTAGE
