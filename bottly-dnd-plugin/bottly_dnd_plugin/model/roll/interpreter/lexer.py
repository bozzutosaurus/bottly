from sly import Lexer


class RollCommandLexer(Lexer):

    tokens = {'COMMAND_KEYWORD', 'WITH_KEYWORD', 'ADVANTAGE_KEYWORD', 'DISADVANTAGE_KEYWORD',
              'DICE', 'NUMBER', 'PLUS', 'MINUS'}
    ignore = ' \t\n'

    COMMAND_KEYWORD = r'/roll'
    WITH_KEYWORD = 'with'
    ADVANTAGE_KEYWORD = 'advantage'
    DISADVANTAGE_KEYWORD = 'disadvantage'

    DICE = r'\d+d\d+'
    NUMBER = r'\d+'
    PLUS = r'\+'
    MINUS = r'-'
