import dataclasses
import typing

from bottly_dnd_plugin.model.roll.roll import Roll, RollArithmetic, RollModifier, RollNumericComponent, \
    RollResult


@dataclasses.dataclass
class RollCommand:
    components: typing.List[typing.Tuple[RollArithmetic, RollNumericComponent]]
    modifier: RollModifier = RollModifier.NONE


@dataclasses.dataclass
class RollCommandResult:
    message: str
    result: RollResult = None
    other_results: typing.List[RollResult] = None
    success: bool = True


class RollCommandRunner:
    def __init__(self):
        self.command_functions: typing.Dict[RollModifier, typing.Callable[[Roll], RollCommandResult]] = {
            RollModifier.NONE: self._roll,
            RollModifier.ADVANTAGE: self._roll_advantage,
            RollModifier.DISADVANTAGE: self._roll_disadvantage
        }

    def run(self, command: RollCommand) -> RollCommandResult:
        try:
            roll = Roll(command.components)
            return self.command_functions[command.modifier](roll)
        except Exception as e:
            return RollCommandResult(f'Roll command failed: {e}', success=False)

    @staticmethod
    def _roll(roll: Roll) -> RollCommandResult:
        roll_result = roll.perform()
        return RollCommandResult(f'{roll_result.resolved_expression.strip()} = {roll_result.result}', result=roll_result)

    @staticmethod
    def _roll_advantage(roll: Roll) -> RollCommandResult:
        roll_results = roll.perform_n_times(2)
        best_result = max(roll_results, key=lambda _roll: _roll.result)
        roll_results.remove(best_result)
        message = f'{best_result.resolved_expression.strip()} = {best_result.result} (vs {roll_results[0].result})'
        return RollCommandResult(message, result=best_result, other_results=roll_results)

    @staticmethod
    def _roll_disadvantage(roll: Roll) -> RollCommandResult:
        roll_results = roll.perform_n_times(2)
        worst_result = min(roll_results, key=lambda _roll: _roll.result)
        roll_results.remove(worst_result)
        message = f'{worst_result.resolved_expression.strip()} = {worst_result.result} (vs {roll_results[0].result})'
        return RollCommandResult(message, result=worst_result, other_results=roll_results)