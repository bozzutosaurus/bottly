import json

from bottly_dnd_plugin.model.bag.bag import Bag
from bottly_dnd_plugin.model.bag.bag import Item


class JsonBagSerializer:

    def read_bag_from_file(self, file_path: str) -> Bag:
        with open(file_path, 'r') as bag_file:
            raw_bag = json.load(bag_file)

        bag = Bag()
        for entry in raw_bag:
            bag.add(Item(entry['name'], description=entry['description']),
                    quantity=entry['quantity'])
        return bag

    def write_bag_to_file(self, bag: Bag, file_path: str):
        contents = []
        for entry in bag.list_content():
            contents.append({
                'name': entry.item.name,
                'quantity': entry.count,
                'description': entry.item.description
            })
        with open(file_path, 'w') as bag_file:
            json.dump(contents, bag_file)
