import dataclasses
import enum
import typing

from bottly_dnd_plugin.model.bag.bag import Bag
from bottly_dnd_plugin.model.bag.item import Item


class BagAction(enum.Enum):
    ADD = enum.auto()
    REMOVE = enum.auto()
    LIST = enum.auto


@dataclasses.dataclass
class BagCommand:
    action: BagAction
    quantity: int = None
    name: str = None
    description: str = None


@dataclasses.dataclass
class BagCommandResult:
    output: str
    success: bool = True


class BagCommandRunner:
    def __init__(self, bag: Bag):
        self.bag = bag
        self.command_functions: typing.Dict[BagAction, typing.Callable[[BagCommand], BagCommandResult]] = {
            BagAction.ADD: self._add_to_bag,
            BagAction.REMOVE: self._remove_from_bag,
            BagAction.LIST: self._list_bag_contents
        }

    def run(self, command: BagCommand) -> BagCommandResult:
        try:
            return self.command_functions[command.action](command)
        except Exception as e:
            return BagCommandResult(f'Command failed: {e}', success=False)

    def _add_to_bag(self, command: BagCommand) -> BagCommandResult:
        self.bag.add(Item(name=command.name, description=command.description), quantity=command.quantity)
        return BagCommandResult(f'Added {command.quantity} {command.name}(s) to the bag')

    def _remove_from_bag(self, command: BagCommand) -> BagCommandResult:
        self.bag.remove(item_name=command.name, quantity=command.quantity, raise_on_insufficient=False)
        return BagCommandResult(f'Removed {command.quantity} {command.name}(s) from the bag')

    def _list_bag_contents(self, _: BagCommand) -> BagCommandResult:
        entries = self.bag.list_content()
        listing = f'Bag contains:\n' + '\n'.join([f'{entry.item.name}: {entry.count} ({entry.item.description})'
                                                  for entry in entries]) + '\n'
        return BagCommandResult(listing)
