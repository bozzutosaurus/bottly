from sly import Lexer


# noinspection PyUnresolvedReferences,PyMethodMayBeStatic
class BagCommandLexer(Lexer):

    tokens = {'COMMAND_KEYWORD', 'ADD_KEYWORD', 'REMOVE_KEYWORD', 'LIST_KEYWORD', 'WORD', 'NUMBER', 'LPAREN', 'RPAREN'}
    ignore = ' \t\n'

    COMMAND_KEYWORD = '/bag'
    ADD_KEYWORD = r'add|insert|deposit'
    REMOVE_KEYWORD = r'remove|take|withdraw'
    LIST_KEYWORD = r'list|check|print|display|show'
    WORD = r'[A-Za-z]+'
    LPAREN = r'\('
    RPAREN = r'\)'

    @_(r'\d+')
    def NUMBER(self, t):
        t.value = int(t.value)
        return t
