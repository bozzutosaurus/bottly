from bottly_dnd_plugin.model.bag.command_runner import BagAction, BagCommand
from bottly_dnd_plugin.model.bag.interpreter.lexer import BagCommandLexer
from sly import Parser


# noinspection PyUnresolvedReferences,PyMethodMayBeStatic
class BagCommandParser(Parser):
    tokens = BagCommandLexer.tokens

    @_('command : COMMAND_KEYWORD list_action')
    def p_command_action(self, p) -> BagCommand:
        return BagCommand(BagAction.LIST)

    @_('command : COMMAND_KEYWORD item_action phrase')
    def p_command_action_phrase(self, p) -> BagCommand:
        return BagCommand(p.item_action, quantity=1, name=p.phrase)

    @_('command : COMMAND_KEYWORD item_action NUMBER phrase')
    def p_command_action_quantity_phrase(self, p) -> BagCommand:
        return BagCommand(p.item_action, quantity=p.NUMBER, name=p.phrase)

    @_('command : COMMAND_KEYWORD item_action phrase description')
    def p_command_action_phrase_description(self, p) -> BagCommand:
        return BagCommand(p.item_action, quantity=1, name=p.phrase, description=p.description)

    @_('command : COMMAND_KEYWORD item_action NUMBER phrase description')
    def p_command_action_quantity_phrase_description(self, p) -> BagCommand:
        return BagCommand(p.item_action, quantity=p.NUMBER, name=p.phrase, description=p.description)

    @_('list_action : LIST_KEYWORD')
    def list_action(self, p) -> BagAction:
        return BagAction.LIST

    @_('item_action : ADD_KEYWORD')
    def add_item_action(self, p) -> BagAction:
        return BagAction.ADD

    @_('item_action : REMOVE_KEYWORD')
    def remove_item_action(self, p) -> BagAction:
        return BagAction.REMOVE

    @_('description : LPAREN phrase RPAREN')
    def p_description(self, p) -> str:
        return p.phrase

    @_('phrase : WORD')
    def p_phrase(self, p) -> str:
        return p.WORD

    @_('phrase : WORD phrase')
    def p_word_phrase(self, p) -> str:
        return f'{p.WORD} {p.phrase}'
