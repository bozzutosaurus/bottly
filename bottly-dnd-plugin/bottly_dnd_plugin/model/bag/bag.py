import typing

from dataclasses import dataclass
from bottly_dnd_plugin.model.bag.item import Item


@dataclass
class BagEntry:
    item: Item
    count: int


class Bag:
    def __init__(self):
        self.content: typing.Dict[str, BagEntry] = {}

    def __eq__(self, other: 'Bag') -> bool:
        return self.content == other.content

    def add(self, item: Item, quantity: int = 1):
        if not self.content.get(item.name):
            self.content[item.name] = BagEntry(item, quantity)
        else:
            self.content[item.name].count += quantity
            self.content[item.name].item.description = item.description

    def remove(self, item_name: str, quantity: int = 1, raise_on_insufficient: bool = True):
        if not self.has(item_name):
            raise BagError(f'Attempted to remove {item_name} from bag, but it was not in the bag')

        entry = self.content[item_name]
        if raise_on_insufficient and quantity > entry.count:
            raise BagError(f'Attempted to remove {quantity} {item_name}s, but the bag contained only {entry.count}')

        if quantity >= entry.count:
            del self.content[item_name]
        else:
            self.content[item_name].count -= quantity

    def has(self, item_name: str) -> bool:
        return bool(self.content.get(item_name))

    def count_of(self, item_name: str) -> int:
        return self.content[item_name].count if self.has(item_name) else 0

    def peek_entry(self, item_name: str) -> typing.Optional[BagEntry]:
        if self.has(item_name):
            return self.content[item_name]
        return None

    def is_empty(self) -> bool:
        return not bool(self.content)

    def list_content(self) -> typing.List[BagEntry]:
        return sorted(list(self.content.values()), key=lambda entry: entry.item.name)


class BagError(Exception):
    pass
