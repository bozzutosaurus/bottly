class Item:

    def __init__(self, name: str, description: str = None):
        self.name = name
        self.description = description

    def __eq__(self, other: 'Item') -> bool:
        return self.name == other.name and self.description == other.description

    def __str__(self) -> str:
        return f'{self.name} ({self.description})'

    def __repr__(self):
        return str(self)
