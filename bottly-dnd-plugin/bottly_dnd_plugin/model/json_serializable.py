import abc
import typing


class JsonSerializable(abc.ABC):

    @abc.abstractmethod
    def to_json_str(self) -> str:
        pass

    @abc.abstractmethod
    def to_json_dict(self) -> typing.Dict:
        pass

    @abc.abstractmethod
    def from_json_str(self, json_str: str):
        pass

    @abc.abstractmethod
    def from_json_dict(self, json: typing.Dict):
        pass
