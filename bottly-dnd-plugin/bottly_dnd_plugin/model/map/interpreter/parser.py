from bottly_dnd_plugin.model.map.command_runner import MapAction, MapCommand
from bottly_dnd_plugin.model.map.interpreter.lexer import MapCommandLexer
from sly import Parser


# noinspection PyUnresolvedReferences,PyMethodMayBeStatic
class MapCommandParser(Parser):
    tokens = MapCommandLexer.tokens

    @_('command : COMMAND_KEYWORD ADD_KEYWORD phrase')
    def add_map(self, p) -> MapCommand:
        return MapCommand(MapAction.ADD, map_name=p.phrase)

    @_('command : COMMAND_KEYWORD DELETE_KEYWORD phrase')
    def delete_map(self, p) -> MapCommand:
        return MapCommand(MapAction.DELETE, map_name=p.phrase)

    @_('command : COMMAND_KEYWORD SHOW_KEYWORD phrase')
    def show_map(self, p) -> MapCommand:
        return MapCommand(MapAction.SHOW, map_name=p.phrase)

    @_('command : COMMAND_KEYWORD LIST_KEYWORD')
    def list_maps(self, p) -> MapCommand:
        return MapCommand(MapAction.LIST)

    @_('phrase : WORD')
    def p_phrase(self, p) -> str:
        return p.WORD

    @_('phrase : WORD phrase')
    def p_word_phrase(self, p) -> str:
        return f'{p.WORD} {p.phrase}'
