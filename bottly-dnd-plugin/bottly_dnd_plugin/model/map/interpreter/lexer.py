from sly import Lexer


# noinspection PyUnresolvedReferences,PyMethodMayBeStatic
class MapCommandLexer(Lexer):

    tokens = {'COMMAND_KEYWORD', 'ADD_KEYWORD', 'DELETE_KEYWORD', 'LIST_KEYWORD', 'SHOW_KEYWORD', 'WORD'}
    ignore = ' \t\n'

    COMMAND_KEYWORD = '/map'
    ADD_KEYWORD = r'add'
    DELETE_KEYWORD = r'delete'
    SHOW_KEYWORD = r'show'
    LIST_KEYWORD = r'list'
    WORD = r'[A-Za-z]+'

