import os
import pathlib
import typing


class Map:
    def __init__(self, name: str, file_path: pathlib.Path):
        self.name = name
        self.file_path = file_path

    def exists(self) -> bool:
        return self.file_path.exists()

    def delete(self):
        if self.exists():
            os.remove(self.file_path)


class MapManifest:

    def __init__(self):
        self.maps: typing.Dict[str, Map] = {}

    @property
    def all_maps(self) -> typing.List[Map]:
        return list(self.maps.values())

    def add_map(self, m: Map):
        self.maps[m.name] = m

    def get_map(self, map_name: str) -> typing.Optional[Map]:
        return self.maps.get(map_name)

    def remove_map(self, map_name: str):
        if self.maps.get(map_name):
            self.maps[map_name].delete()
            del self.maps[map_name]
