import dataclasses
import enum
import pathlib
import typing

from bottly_dnd_plugin.model.map.map import MapManifest, Map


class MapAction(enum.Enum):
    ADD = enum.auto()
    DELETE = enum.auto()
    LIST = enum.auto()
    SHOW = enum.auto()


@dataclasses.dataclass
class MapCommand:
    action: MapAction
    map_name: str = None
    map_file_path: pathlib.Path = None


@dataclasses.dataclass
class MapCommandResult:
    message: str
    map: Map = None
    success: bool = True


class MapCommandRunner:
    def __init__(self, map_manifest: MapManifest):
        self.map_manifest = map_manifest
        self.command_functions: typing.Dict[MapAction, typing.Callable[[MapCommand], MapCommandResult]] = {
            MapAction.ADD: self._add_map,
            MapAction.DELETE: self._delete_map,
            MapAction.LIST: self._list_maps,
            MapAction.SHOW: self._show_map
        }

    def run(self, command: MapCommand) -> MapCommandResult:
        try:
            return self.command_functions[command.action](command)
        except Exception as e:
            return MapCommandResult(f'Command failed: {e}', success=False)

    def _add_map(self, command: MapCommand) -> MapCommandResult:
        new_map = Map(command.map_name, command.map_file_path)
        self.map_manifest.add_map(new_map)
        message = f'Added a new map named "{new_map.name}"'
        return MapCommandResult(message, map=new_map)

    def _delete_map(self, command: MapCommand) -> MapCommandResult:
        if self.map_manifest.get_map(command.map_name):
            self.map_manifest.remove_map(command.map_name)
            message = f'Deleted the map named "{command.map_name}"'
        else:
            message = f'No map named "{command.map_name}" to delete!'
        return MapCommandResult(message)

    def _list_maps(self, _: MapCommand) -> MapCommandResult:
        message = 'Maps:\n' + '\n'.join([m.name for m in self.map_manifest.all_maps])
        return MapCommandResult(message)

    def _show_map(self, command: MapCommand) -> MapCommandResult:
        success = True
        map_to_show = self.map_manifest.get_map(command.map_name)
        if not map_to_show:

            message = f'No map named "{command.map_name}" to show!'
            success = False
        else:
            message = f'Retrieving map "{command.map_name}"'
        return MapCommandResult(message, success=success, map=map_to_show)
