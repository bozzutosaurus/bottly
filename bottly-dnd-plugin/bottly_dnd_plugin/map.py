import discord
import os
import pathlib
import typing

from discord.ext import commands
from bottly.discord_bot_plugin import DiscordBotMessagePlugin
from bottly.discord_plugin_actions import PostMessage, UploadFile, DiscordPluginAction, DownloadFile
from bottly_dnd_plugin.model.map.command_runner import MapAction, MapCommand, MapCommandResult, \
    MapCommandRunner
from bottly_dnd_plugin.model.map.interpreter.lexer import MapCommandLexer
from bottly_dnd_plugin.model.map.interpreter.parser import MapCommandParser
from bottly_dnd_plugin.model.map.map import MapManifest


class MapMessagePlugin(DiscordBotMessagePlugin):

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.map_directory = pathlib.Path('/tmp/maps')
        os.makedirs(self.map_directory, exist_ok=True)
        self.map_manifest = MapManifest()
        self.map_command_lexer = MapCommandLexer()
        self.map_command_parser = MapCommandParser()
        self.map_command_runner = MapCommandRunner(self.map_manifest)

    @commands.command(name='map')
    async def map(self, context: commands.Context):
        actions: typing.List[DiscordPluginAction] = []
        command = self.map_command_parser.parse(self.map_command_lexer.tokenize(context.message.content))

        # Pre-process command
        if command.action == MapAction.ADD:
            if not context.message.attachments:
                await self.perform_actions([PostMessage('ADD command requires a file attachment')], context)
                return
            else:
                actions.append(self._prepare_map_download(command, context.message))

        # Process command
        result = self.map_command_runner.run(command)
        actions.append(PostMessage(result.message))

        # Post-process command
        if command.action == MapAction.SHOW and result.success:
            actions.append(self._prepare_map_upload(result))

        await self.perform_actions(actions, context)

    def _prepare_map_download(self, command: MapCommand, message: discord.Message) -> DownloadFile:
        _, file_type = os.path.splitext(message.attachments[0].filename)
        map_file_path = (self.map_directory / command.map_name.replace(' ', '_')).with_suffix(file_type)
        command.map_file_path = map_file_path
        return DownloadFile(message.attachments[0], map_file_path)

    @staticmethod
    def _prepare_map_upload(command_result: MapCommandResult) -> UploadFile:
        _, file_type = os.path.splitext(command_result.map.file_path)
        attachment_name = f"{command_result.map.name.replace(' ', '_')}{file_type}"
        return UploadFile(command_result.map.file_path, attachment_name=attachment_name)
