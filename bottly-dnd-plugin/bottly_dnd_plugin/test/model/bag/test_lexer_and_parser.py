import pytest

from bottly_dnd_plugin.model.bag.command_runner import BagAction, BagCommand
from bottly_dnd_plugin.model.bag.interpreter.lexer import BagCommandLexer
from bottly_dnd_plugin.model.bag.interpreter.parser import BagCommandParser


@pytest.fixture(scope='class')
def lexer() -> BagCommandLexer:
    return BagCommandLexer()


@pytest.fixture(scope='class')
def parser() -> BagCommandParser:
    return BagCommandParser()


def parse(data: str, lexer: BagCommandLexer, parser: BagCommandParser) -> BagCommand:
    return parser.parse(lexer.tokenize(data))


class TestBagCommandLexerAndParser:

    @pytest.mark.parametrize('statement, expected_command', [
        # ADD
        pytest.param('/bag add coin', BagCommand(BagAction.ADD, quantity=1, name='coin'),
                     id='add implicit single item'),
        pytest.param('/bag add 1 coin', BagCommand(BagAction.ADD, quantity=1, name='coin'),
                     id='add explicit single item'),
        pytest.param('/bag add coin (metal)', BagCommand(BagAction.ADD, quantity=1, name='coin', description='metal'),
                     id='add implicit single item with short description'),
        pytest.param('/bag add tea (a b c)', BagCommand(BagAction.ADD, quantity=1, name='tea', description='a b c'),
                     id='add implicit single item with long description'),
        pytest.param('/bag add old nice coin', BagCommand(BagAction.ADD, quantity=1, name='old nice coin'),
                     id='add implicit single item with long name'),
        pytest.param('/bag add 5 coin', BagCommand(BagAction.ADD, quantity=5, name='coin'),
                     id='add multiple items'),
        pytest.param('/bag add 5 coin (shiny)', BagCommand(BagAction.ADD, quantity=5, name='coin', description='shiny'),
                     id='add multiple items with description'),
        # REMOVE
        pytest.param('/bag remove coin', BagCommand(BagAction.REMOVE, quantity=1, name='coin'),
                     id='add implicit single item'),
        pytest.param('/bag remove 1 coin', BagCommand(BagAction.REMOVE, quantity=1, name='coin'),
                     id='add explicit single item'),
        pytest.param('/bag remove coin (metal)', BagCommand(BagAction.REMOVE, quantity=1, name='coin',
                                                            description='metal'),
                     id='add implicit single item with short description'),
        pytest.param('/bag remove tea (a b c)', BagCommand(BagAction.REMOVE, quantity=1, name='tea',
                                                           description='a b c'),
                     id='add implicit single item with long description'),
        pytest.param('/bag remove old nice coin', BagCommand(BagAction.REMOVE, quantity=1, name='old nice coin'),
                     id='add implicit single item with long name'),
        pytest.param('/bag remove 5 coin', BagCommand(BagAction.REMOVE, quantity=5, name='coin'),
                     id='add multiple items'),
        pytest.param('/bag remove 5 coin (shiny)', BagCommand(BagAction.REMOVE, quantity=5, name='coin',
                                                              description='shiny'),
                     id='add multiple items with description'),
        # LIST
        pytest.param('/bag list', BagCommand(BagAction.LIST),
                     id='single word interpreter'),
    ])
    def test_valid_commands(self, lexer: BagCommandLexer, parser: BagCommandParser, statement: str,
                            expected_command: BagCommand):
        assert expected_command == parse(statement, lexer, parser)

    @pytest.mark.parametrize('statement', [
        pytest.param('6', id='non word interpreter'),
        pytest.param('add (sorta smelly) socks', id='description before item name'),
        pytest.param('destroy', id='bad interpreter')
    ])
    def test_invalid_commands(self, lexer: BagCommandLexer, parser: BagCommandParser, statement: str):
        assert not parse(statement, lexer, parser)
