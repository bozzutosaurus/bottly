import pytest

from bottly_dnd_plugin.model.bag.bag import Bag, BagError
from bottly_dnd_plugin.model.bag.item import Item


@pytest.fixture(scope='function')
def bag() -> Bag:
    return Bag()


coin = Item('GP', description='Gold coin')
trophy = Item('Trophy')
potion = Item('Potion of Lesser Healing', description='Heals one d4 of health')


class TestBag:

    def test_bag_starts_as_empty(self, bag: Bag):
        assert bag.is_empty()

    def test_bag_not_empty_after_adding_item(self, bag: Bag):
        bag.add(potion)
        assert not bag.is_empty()

    def test_bag_has_item_after_adding(self, bag: Bag):
        bag.add(potion)
        assert bag.has(potion.name)

    def test_count_of_single_item(self, bag: Bag):
        bag.add(trophy)
        assert bag.count_of(trophy.name) == 1

    def test_count_of_multiple_items(self, bag: Bag):
        bag.add(potion, quantity=4)
        assert bag.count_of(potion.name) == 4

    def test_peek_nonexistent_item(self, bag: Bag):
        assert not bag.peek_entry('riot juice')

    def test_peek_real_item(self, bag: Bag):
        bag.add(trophy)
        assert trophy == bag.peek_entry(trophy.name).item

    def test_count_of_item_not_in_bag(self, bag: Bag):
        assert bag.count_of('nothing') == 0

    def test_error_on_removing_nonexistent_item(self, bag: Bag):
        with pytest.raises(BagError):
            bag.remove(trophy.name)

    def test_error_on_removing_too_many(self, bag: Bag):
        bag.add(coin, quantity=5)
        with pytest.raises(BagError):
            bag.remove(coin.name, quantity=10)

    def test_no_error_on_too_many_if_insufficient_allowed(self, bag: Bag):
        bag.add(coin, quantity=5)
        bag.remove(coin.name, quantity=10, raise_on_insufficient=False)
        assert bag.is_empty()

    def test_remove_some(self, bag: Bag):
        bag.add(coin, quantity=5)
        bag.remove(coin.name, quantity=3)
        assert not bag.is_empty()

    def test_bag_is_empty_after_removing(self, bag: Bag):
        bag.add(potion)
        bag.remove(potion.name)
        assert bag.is_empty()

    def test_add_multiple_of_same_item(self, bag: Bag):
        bag.add(coin, quantity=5)
        assert bag.has(coin.name)
        assert len(bag.list_content()) == 1

    def test_add_repeat_of_same_item(self, bag: Bag):
        bag.add(coin, quantity=2)
        bag.add(coin, quantity=3)
        assert bag.list_content()[0].count == 5

    def test_add_multiple_different_items(self, bag: Bag):
        bag.add(potion)
        bag.add(trophy)
        assert len(bag.list_content()) == 2
