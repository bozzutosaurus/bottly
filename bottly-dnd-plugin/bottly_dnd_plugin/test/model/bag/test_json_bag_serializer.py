import os
import pytest
import tempfile

from bottly_dnd_plugin.model.bag.bag import Bag
from bottly_dnd_plugin.model.bag.item import Item
from bottly_dnd_plugin.model.bag.serializer.json import JsonBagSerializer


@pytest.fixture(scope='function')
def loaded_bag() -> Bag:
    bag = Bag()
    bag.add(Item('cheese', description='fine aged cheddar'))
    bag.add(Item('beer', description='4.5% ABV'), quantity=6)
    bag.add(Item('knife', description='A sharp knife'))
    return bag


class TestJsonBagSerializer:

    def test_bag_is_same_after_serialization(self, loaded_bag):
        with tempfile.TemporaryDirectory() as temporary_directory:
            bag_file_path = os.path.join(temporary_directory, 'bag.json')
            bag_serializer = JsonBagSerializer()
            bag_serializer.write_bag_to_file(loaded_bag, bag_file_path)
            new_bag = bag_serializer.read_bag_from_file(bag_file_path)

        assert new_bag == loaded_bag