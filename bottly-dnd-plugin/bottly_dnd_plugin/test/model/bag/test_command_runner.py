import pytest

from bottly_dnd_plugin.model.bag.bag import Bag
from bottly_dnd_plugin.model.bag.command_runner import BagAction, BagCommand, BagCommandRunner
from bottly_dnd_plugin.model.bag.item import Item


@pytest.fixture(scope='function')
def bag() -> Bag:
    return Bag()


@pytest.fixture(scope='function')
def runner(bag: Bag) -> BagCommandRunner:
    return BagCommandRunner(bag)


class TestBagCommandRunner:

    def test_add_to_bag(self, bag: Bag, runner: BagCommandRunner):
        command = BagCommand(BagAction.ADD, name='chisel', quantity=1, description='a small tool')
        assert runner.run(command).success
        assert bag.has('chisel')
        assert bag.count_of('chisel') == 1
        assert bag.peek_entry('chisel').item.description == 'a small tool'

    def test_remove_from_bag(self, bag: Bag, runner: BagCommandRunner):
        command = BagCommand(BagAction.REMOVE, name='shovel', quantity=1)
        bag.add(Item('shovel'))
        assert runner.run(command).success
        assert not bag.has('shovel')

    def test_list_bag(self, runner: BagCommandRunner):
        command = BagCommand(BagAction.LIST)
        assert runner.run(command).success

    def test_bad_command_raises_error(self, runner: BagCommandRunner):
        command = BagCommand(None)
        assert not runner.run(command).success
