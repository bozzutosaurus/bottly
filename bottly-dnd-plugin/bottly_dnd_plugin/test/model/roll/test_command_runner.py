import pytest

from bottly_dnd_plugin.model.roll.command_runner import  RollCommand, RollCommandRunner
from bottly_dnd_plugin.model.roll.interpreter.lexer import RollCommandLexer
from bottly_dnd_plugin.model.roll.interpreter.parser import RollCommandParser


@pytest.fixture(scope='class')
def lexer() -> RollCommandLexer:
    return RollCommandLexer()


@pytest.fixture(scope='class')
def parser() -> RollCommandParser:
    return RollCommandParser()


@pytest.fixture(scope='class')
def runner() -> RollCommandRunner:
    return RollCommandRunner()


def get_command(roll: str, lexer: RollCommandLexer, parser: RollCommandParser) -> RollCommand:
    return parser.parse(lexer.tokenize(roll))


class TestRollCommandRunner:

    def test_roll_single_dice(self, lexer: RollCommandLexer, parser: RollCommandParser, runner: RollCommandRunner):
        assert 1 <= runner.run(get_command('/roll 1d4', lexer, parser)).result.result <= 4

    def test_roll_multiple_dice(self, lexer: RollCommandLexer, parser: RollCommandParser, runner: RollCommandRunner):
        assert 0 <= runner.run(get_command('/roll 1d4 + 1d6 - 1d2', lexer, parser)).result.result <= 9

    def test_roll_with_constants(self, lexer: RollCommandLexer, parser: RollCommandParser, runner: RollCommandRunner):
        assert 6 == runner.run(get_command('/roll 5 + 3 - 2', lexer, parser)).result.result

    def test_roll_mixed(self, lexer: RollCommandLexer, parser: RollCommandParser, runner: RollCommandRunner):
        assert 3 <= runner.run(get_command('/roll 1d2 + 2', lexer, parser)).result.result <= 4

    def test_roll_advantage(self, lexer: RollCommandLexer, parser: RollCommandParser, runner: RollCommandRunner):
        command_result = runner.run(get_command('/roll 1d20 with advantage', lexer, parser))
        assert command_result.result.result >= command_result.other_results[0].result

    def test_roll_disadvantage(self, lexer: RollCommandLexer, parser: RollCommandParser, runner: RollCommandRunner):
        command_result = runner.run(get_command('/roll 1d20 with disadvantage', lexer, parser))
        assert command_result.result.result <= command_result.other_results[0].result

    # def test_roll_starting_with_symbolic_raises_error(self, lexer: RollCommandLexer, parser: RollCommandParser, runner: RollCommandRunner):
    #     with pytest.raises(DiceRollError):
    #         interpreter.roll('+ 1d4')
    #
    # def test_roll_ending_in_symbolic_raises_error(self, interpreter: DiceActionInterpreter):
    #     with pytest.raises(DiceRollError):
    #         interpreter.roll('1d4 +')
    #
    # def test_roll_with_sequential_numeric_raises_error(self, interpreter: DiceActionInterpreter):
    #     with pytest.raises(DiceRollError):
    #         interpreter.roll('1d4 + 1d6 1d4')
    #
    # def test_roll_with_sequential_symbol_raises_error(self, interpreter: DiceActionInterpreter):
    #     with pytest.raises(DiceRollError):
    #         interpreter.roll('1d4 + 1d6 + - 1d4')
