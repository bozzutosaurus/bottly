import typing
from discord.ext import commands

from bottly.discord_bot_plugin import DiscordBotMessagePlugin
from bottly_dnd_plugin.bag import BagMessagePlugin
from bottly_dnd_plugin.map import MapMessagePlugin
from bottly_dnd_plugin.roll import RollMessagePlugin


def get_plugins(bot: commands.Bot) -> typing.List[DiscordBotMessagePlugin]:
    return [
        BagMessagePlugin(bot),
        MapMessagePlugin(bot),
        RollMessagePlugin(bot)
    ]