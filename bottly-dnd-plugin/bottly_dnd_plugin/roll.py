from discord.ext import commands
from bottly.discord_bot_plugin import DiscordBotMessagePlugin
from bottly.discord_plugin_actions import PostMessage
from bottly_dnd_plugin.model.roll.command_runner import RollCommandRunner
from bottly_dnd_plugin.model.roll.interpreter.lexer import RollCommandLexer
from bottly_dnd_plugin.model.roll.interpreter.parser import RollCommandParser


class RollMessagePlugin(DiscordBotMessagePlugin):

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.roll_command_lexer = RollCommandLexer()
        self.roll_command_parser = RollCommandParser()
        self.roll_command_runner = RollCommandRunner()

    @commands.command(name='roll')
    async def roll(self, context: commands.Context):
        command = self.roll_command_parser.parse(self.roll_command_lexer.tokenize(context.message.content))
        command_result = self.roll_command_runner.run(command)
        await self.perform_actions([PostMessage(command_result.message)], context)
