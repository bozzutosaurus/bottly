from setuptools import setup

setup(
    name='bottly-dnd-plugin',
    version='1.0',
    description='A set of DnD tools designed for bottly',
    author='Dom Bozzuto',
    author_email='domeniclbozzuto@gmail.com',
    packages=['bottly_dnd_plugin'],
    requires=['bottly', 'discord', 'pytest', 'sly'],
    entry_points={'bottly.plugins': 'dnd_plugin = bottly_dnd_plugin'}
)